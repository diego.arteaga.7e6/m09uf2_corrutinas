import kotlinx.coroutines.*

fun main() {
    println("Inicio del programa")
    GlobalScope.launch {
        repeat(4) { i ->
            println("$i: Hello World!")
            delay(100)
        }
    }
    runBlocking {
        delay(500)
    }
    println("Finished!")
}