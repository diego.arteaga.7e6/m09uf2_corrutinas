import kotlinx.coroutines.*

suspend fun main(){
doExe1p3()
}

suspend fun doExe1p3() {
    withContext(Dispatchers.Default){
        launch {
            println("Background processing started")
            delay(1000)
            println("Background processing finished")
        }
        println("ok!")
    }
}
