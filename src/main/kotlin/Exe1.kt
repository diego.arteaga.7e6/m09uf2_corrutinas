import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    println("With t = 1500:")
    x(1500)
    println("-----")
    println("With t = 900:")
    x(900)
}

fun x(t: Int) {


    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(t.toLong())
        println("The main program is finished")
    }
}
/*
t= 1500
The main program is started
The main program continues
Background processing started
Background processing finished
The main program is finished
t = 900
The main program is started
The main program continues
Background processing started
The main program is finished
 */