import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(){
doBackground()
}
fun y(t: Int) {

    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(t.toLong())
        println("The main program is finished")
    }
}

fun doBackground() = runBlocking {
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }


}