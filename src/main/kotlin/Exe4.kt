import kotlinx.coroutines.*
import kotlin.random.Random

fun main() = runBlocking {
    val secretNumber = Random.nextInt(1, 51) // Genera el número secreto aleatoriamente
    var guess: Int? = null
    var timeUp = false

    // Corrutina que cuenta el tiempo límite
    launch {
        delay(10000)
        timeUp = true
    }

    // Corrutina que maneja las respuestas del usuario
    launch {
        while (guess != secretNumber && !timeUp) {
            print("Adivina el número secreto entre 1 y 50: ")
            guess = readLine()?.toIntOrNull()

            if (guess == null) {
                println("Eso no es un número válido, intenta de nuevo")
            } else if (guess < secretNumber) {
                println("Demasiado bajo, intenta de nuevo")
            } else if (guess > secretNumber) {
                println("Demasiado alto, intenta de nuevo")
            }
        }

        if (!timeUp) {
            println("¡Adivinaste el número secreto!")
        } else {
            println("¡Se acabó el tiempo! El número secreto era $secretNumber")
        }
    }
}