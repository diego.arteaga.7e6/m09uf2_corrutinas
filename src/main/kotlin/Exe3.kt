import kotlinx.coroutines.*
import java.io.File
import kotlin.system.measureTimeMillis
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


suspend fun playSong(lyrics: List<String>) {
    for (line in lyrics) {
        println(line)
        delay(3000) // 3 segundos de espera entre líneas
    }
}

fun main() {
    var time: Long = 0
    val lyrics = File("./src/data/lyrics.txt").readLines()

    time = measureTimeMillis {
        runBlocking {
            launch { playSong(lyrics) }
            delay(lyrics.size * 3000L) // Espera hasta que se reproduzca toda la canción
        }
    }

    println("Tiempo transcurrido: ${time/1000} segundos")
}
