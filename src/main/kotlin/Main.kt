import kotlinx.coroutines.*


fun main() = runBlocking {
    doWorld()
    println("Done!")
}

suspend fun doWorld() = coroutineScope {
    launch {
        delay(1000)
        println("World!")
    }
    println("Hello")
}
