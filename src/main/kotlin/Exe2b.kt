import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.GlobalScope

suspend fun main() {
    println("Inicio del programa")
    printHelloWorld()
    println("Finished!")
}

suspend fun printHelloWorld() {
    GlobalScope.launch {
        repeat(4) { i ->
            println("$i: Hello World!")
            delay(100)
        }
    }
    delay(500)
}